import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';

import { BlogApp } from './app';

ReactDOM.render(<BlogApp />, document.getElementById('root'));