import { Layout, Typography } from 'antd';

export const BlogHeader = () => {
    return (
        <Layout.Header>
            <Typography.Title level={2} type="danger">
                Blog A Mini Microservices App
            </Typography.Title>
        </Layout.Header>
    );
};