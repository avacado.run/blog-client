import axios from 'axios';

import { Button, Col, Form, Input, Typography } from 'antd';

export const PostCreate = ({ reload, reloadTrigger }) => {
    const { Title } = Typography;

    const [form] = Form.useForm();

    const onFinish = async (values) => {
        await axios.post('http://localhost:4000/posts', {
            title: values.createPost,
        });
        form.resetFields();
        reload(!reloadTrigger);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <>
            <Col offset={1} span={10}>
                <Title level={3} type="danger">
                    Create Post
                </Title>
                <Form
                    form={form}
                    name="basic"
                    wrapperCol={{ offset: 0, span: 24 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        name="createPost"
                        rules={[
                            { required: true, message: 'Please input post!' },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 0, span: 16 }}>
                        <Button
                            type="primary"
                            htmlType="submit"
                            style={{ borderRadius: '10px' }}
                        >
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Col>
        </>
    );
};