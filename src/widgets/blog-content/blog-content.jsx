import { Layout, Row } from 'antd';

import { PostCreate } from 'features/post-create';
import { PostList } from 'features/post-list';

export const BlogContent = ({ posts, reload, reloadTrigger }) => {
    return (
        <Layout.Content
            style={{
                height: '80vh',
                overflow: 'hidden',
            }}
        >
            <Row style={{ display: 'flex', flexDirection: 'column' }}>
                <PostCreate reload={reload} reloadTrigger={reloadTrigger} />
                <PostList
                    posts={posts}
                    reload={reload}
                    reloadTrigger={reloadTrigger}
                />
            </Row>
        </Layout.Content>
    );
};
