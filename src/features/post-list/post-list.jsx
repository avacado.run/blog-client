import { Card, Col, Space, Typography } from 'antd';

import { CommentCreate } from './comment-create';
import { CommentList } from './comment-list';
import { statuses } from './model';

export const PostList = ({ posts, reload, reloadTrigger }) => {
    const { Title } = Typography;
    const commentListStyle = {
        width: '100%',
        height: '250px',
    };
    const gridStyle = {
        width: '100%',
        height: '190px',
    };

    const renderedPosts = Object.values(posts).map((post, index) => (
        <Card
            title={post.title}
            style={{ width: 600, height: 500, borderRadius: '10px' }}
            key={post.id}
        >
            <Card.Grid style={commentListStyle} hoverable={false}>
                <p>Comments:</p>
                <CommentList comments={post.comments} statuses={statuses} />
            </Card.Grid>
            <Card.Grid style={gridStyle} hoverable={false}>
                <CommentCreate
                    postId={post.id}
                    reload={reload}
                    reloadTrigger={reloadTrigger}
                />
            </Card.Grid>
        </Card>
    ));

    return (
        <Col offset={1} span={24}>
            <Title level={3} type="danger">
                Posts
            </Title>
            <Space
                direction="horizontal"
                align="center"
                wrap
                size="large"
                style={{
                    height: '55vh',
                    overflow: 'auto',
                }}
            >
                {renderedPosts}
            </Space>
        </Col>
    );
};
