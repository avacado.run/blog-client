import { Button, Col, Form, Input } from 'antd';
import axios from 'axios';

export const CommentCreate = ({ postId, reload, reloadTrigger }) => {
    const [form] = Form.useForm();

    const onFinish = async (values) => {
        await axios.post(`http://localhost:4001/posts/${postId}/comments`, {
            content: values.createComment,
        });
        form.resetFields();
        reload(!reloadTrigger);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <>
            <Col span={24}>
                <Form
                    form={form}
                    name="basic"
                    labelCol={{ span: 24 }}
                    wrapperCol={{ offset: 0, span: 24 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label="Add comment"
                        name="createComment"
                        rules={[
                            {
                                required: true,
                                message: 'Please input comment!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 0, span: 16 }}>
                        <Button
                            type="primary"
                            htmlType="submit"
                            style={{ borderRadius: '10px' }}
                        >
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Col>
        </>
    );
};
