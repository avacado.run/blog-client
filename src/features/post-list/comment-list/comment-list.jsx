import { List, Typography } from 'antd';

export const CommentList = ({ comments, statuses }) => {
    const rendererComments = (
        <List
            style={{
                height: '170px',
                overflow: 'auto',
            }}
            bordered
            size="small"
            dataSource={comments}
            renderItem={(item) => (
                <List.Item>
                    <Typography.Text type={statuses[item.status].type}>
                        {item.status === 'approved'
                            ? item.content
                            : statuses[item.status].content}
                    </Typography.Text>
                </List.Item>
            )}
        />
    );

    return <>{rendererComments}</>;
};
