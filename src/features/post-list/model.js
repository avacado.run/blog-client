export const statuses = {
    pending: {
        content: 'This comment awaiting moderation',
        type: 'warning',
    },
    approved: {
        type: 'success',
    },
    rejected: {
        content: 'This comment was rejected',
        type: 'danger',
    },
};