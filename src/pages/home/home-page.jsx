import { Layout, Typography } from 'antd';
import { useEffect, useState } from 'react';
import axios from 'axios';

import { BlogHeader } from 'widgets/blog-header';
import { BlogContent } from 'widgets/blog-content';

export const HomePage = () => {
    const { Footer } = Layout;
    const { Title } = Typography;

    const [posts, setPosts] = useState({});
    const [reloadTrigger, setTrigger] = useState(false);

    const reload = (trigger) => {
        setTrigger(trigger);
    };

    const fetchPosts = async () => {
        const res = await axios.get('http://localhost:4002/posts');

        setPosts(res.data);
    };

    useEffect(() => {
        fetchPosts();
    }, [reloadTrigger]);

    return (
        <Layout style={{ minHeight: '100vh' }}>
            <BlogHeader />
            <BlogContent
                posts={posts}
                reload={reload}
                reloadTrigger={reloadTrigger}
            />
            <Footer>
                <Title level={5}>Blog A Mini Microservices App ©2021</Title>
            </Footer>
        </Layout>
    );
};
