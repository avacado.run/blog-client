import { Layout, Typography } from 'antd';

export const BlogFooter = () => (
    <Layout.Footer>
        <Typography.Title level={5}>
            Blog A Mini Microservices App ©2021
        </Typography.Title>
    </Layout.Footer>
);